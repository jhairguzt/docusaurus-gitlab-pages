---
id: admin-sync
title: Sincronización de datos
sidebar_label: Sincronización de datos
slug: /sync-general
---

Para sincronizar un archivo de beneficiarios, puntos de retiro o zonas de contagio debe dirigirse a la tabla de sincronización respectiva. Por ejemplo, si quiere sincronizar un archivo de beneficiarios debe dirijirse a la pantalla de beneficiarios sincronizados.

En cada una de ellas al final de la tabla se encuentra un botón para sincronizar los archivos, la cual al hacer click se les redirigirá a una pantalla común para los tres tipos de sincronización tal y como se muestra en la siguiente imagen.

![img](../../../../static/img/admin/sync/Admin-sync-generic.png)

En este se muestra un boton para descargar plantillas, una zona de información del archivo que se va a subir y la zona de subida de archivo.

## Descargar plantillas
Para descargar las plantillas del formato de cómo deben se subido los archivos debe hacer click en el botón de Plantilla de archivo que se encuentra en la esquina derecha superior. Se desplegará tres opciones.
- Plantilla para beneficiarios
- Plantilla para puntos de retiro
- Plantilla para las zonas de contagio

Al seleccionar una de ellas se descargará la plantilla en formato ``.csv``

## Cargar un archivo
Para cargar un archivo debe arrastrar el archivo hasta la zona de subida o hacer click en la zona de subida y buscar el archivo. Una vez el archivo se haya cargado aparecerá un botón para iniciar la carga de datos.

![img](../../../../static/img/admin/sync/Admin-sync-generic-init-upload.png)

El estado y detalle de la carga se mostrará en el historial de sincronización.

:::caution
En el caso que el archivo no está en el formato de ``.csv`` o no tiene el formato de alguna plantilla no se podrá iniciar la carga pues no es posible detectar el tipo de dato a sincronizar.
:::

![img](../../../../static/img/admin/sync/Admin-generic-init-upload-fail.png)
