---
id: admin-history
title: Historial de sincronización
sidebar_label: Historial de sincronización
slug: /sync-history
---

El historial de sincronización muestra todas las sincronizaciones realizadas. Este detalla los siguientes datos en una tabla:
- __Código__ de la sincronización
- El __Responsable__ que realizó la sincronización
- __Un resultado general__ de la sincronización (Número de registros sincronizados)
- __Fecha de la sincronización__
- El __Recurso__ sincronizado (Beneficiarios, Puntos de retiro, Zonas de contagio)
- El __Estado__ de la sincronización (Progreso o Completado)

## Visualizar el historial de sincronización
Para ver la tabla de sincronización primero debe hacer click en el boton de sincronización de la barra lateral, eso desplegará nuevas opciones en el menu lateral. Luego debe hacer click en Historial para ver la tabla con el histórico de sincronizaciones

![img](../../../../static/img/admin/sync/Admin-History.png)

## Visualizar el detalle de la sincronización
Para ver el detalle de la sincronización debe dirigirse al historial de sincronizaciónes (Ver el cápitulo anterior). Luego debe hacer click sobre un sincronización en específico. Se redirigirá hacia una página con el detalle de la sincronización seleccionada como el siguiente ejemplo.

![img](../../../../static/img/admin/sync/Admin-History-detail.png)

En esta pantalla se muestra el estado de la sincronización, el tipo de sincronización, el responsable de la sincronización y la fecha en la que se llevó a cabo. Adicionalmente, se muestran el número de datos cargados y el número de datos que no fueron cargados por algún error.