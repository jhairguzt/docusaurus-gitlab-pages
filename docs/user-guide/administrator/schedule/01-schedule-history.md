---
id: admin-schedule-history
title: Historial de cronogramas
sidebar_label: Historial de cronogramas
slug: /schedule-history
---

## Visualizar el historial de cronogramas
Para ver la tabla con el historico de cronogramas generados primero debe hacer click en el boton de cronograma de la barra lateral, eso desplegará nuevas opciones en el menu lateral. Luego debe hacer click en Historial de cronogramas para ver la tabla con los cronogramas generados. Este se divide en dos pestañas: Oficiales y Pruebas como se muestra en el ejemplo.

![img](../../../../static/img/admin/schedule/Admin-schedule-history.png)

## Ver detalle de un cronograma
Para ver el detalle de un cronograma previamente necesita ingresar al historial de cronogramas (Ver el capítulo anterior). Luego hacer click sobre uno de los cronogramas para ser redirigido a una nueva pantalla con el detalle de este como se muestra en la siguiente imagen.

![img](../../../../static/img/admin/schedule/Admin-schedule-history-detail.png)

En esta pantalla se ve una barra de color azul que indica cuantos días han transcurrido desde el primer día. Luego se muestra un recudro gris con el responsable de ese cronograma, la fecha en la que fue creada, el número de beneficiarios participantes y el número de puntos de retiro participantes.

En la siguiente área se muestra tres pestañas (Detalles de beneficiarios, Detalle de puntos de retiro y Detalles) en cada uno de ellos se muestra cierta información que luego puede ser exportada en ``.csv`` o ``.pdf``-