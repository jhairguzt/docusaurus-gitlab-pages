---
id: beneficiary-report-an-incident
title: Reportar una incidencia
sidebar_label: Reportar una incidencia
slug: /beneficiary-report-an-incident
---

Guía para reportar una incidencia

## Reportar una incidencia
Si es beneficiario y desea reportar una incidencia puede ingresar desde [aquí](http://swing-web-client-release.eba-3matrg2z.us-east-1.elasticbeanstalk.com/bonus/incident) o hacer click en el botón de reportar incidencia. Se mostrará un formulario como el siguiente.

![img](../../../static/img/beneficiary/Beneficiario-Incidentes-Sistema-Swing.png)
Para enviar una incidencia se necesita lo siguiente:
- Completar Código de familia
- Seleccionar un tipo de incidencia
- Completar el campo de Mensaje con una pequeña descripción sobre lo ocurrido

![img](../../../static/img/beneficiary/Beneficiario-Incidentes-Formulario-Sistema-Swing.png)

Si el código es correcto la inicidencia se registrará sin nigún problema.

![img](../../../static/img/beneficiary/Beneficiario-Incidente-Satisfactorio-Sistema-Swing.png)

:::caution
En el caso el código ingresado es incorrecto o no es beneficiario del cronograma actual se mostrará un error.
:::

## Elegir punto de retiro

Un tipo de inicidencia especial definido por el sistema permite elegir puntos de retiro para que pueda ser considerado por el próximo cronograma.

Para ello debe selección el tipo de incidencia definido por el sistema y completar el formulario indicado en reportar inicidencia. Luego al momento de envíar la inicidencia se le consultará si es que desea elegir su punto de retiro. 

![img](../../../static/img/beneficiary/Beneficiario-Incidente-Envio-Especial-Sistema-Swing.png)

Si elige que ``No``, se enviará solo la incidencia, pero si elige ``Si`` se redirigirá a una página como se muestra en la siguiente imagen.

![img](../../../static/img/beneficiary/Beneficiario-Elegir-puntos-de-retiro-Sistema-Swing.png)

Para elegir los nuevos puntos de retiro deberá serguir los siguientes pasos

1. Debe completar:
    - Código de familia
    - Elegir un departamento
    - Elegir una provincia
    - Elegir un distrito

<p align="center">
<img
  src={require('../../../static/img/beneficiary/Beneficiario-Form-Punto-Retiro-Sistema-Swing.png').default}
  alt="Form punto de retiro"
/>
</p>

2. Click en Buscar puntos de retiro
3. Seleccionar menos de 3 puntos de retiro

![img](../../../static/img/beneficiary/Beneficiario-Elegir-puntos-de-retiro-Seleccionar-Sistema-Swing.png)

4. Click en el botón seleccionar puntos de retiro para registrarlos

Si el código de familia es correcto se registrará correctamente los puntos de retiro seleccionados
![img](../../../static/img/beneficiary/Beneficiario-Elegir-puntos-de-retiro-Satisfactoria-Sistema-Swing.png)

:::caution
En el caso el código ingresado es incorrecto o no es beneficiario del cronograma actual se mostrará un error.
:::

